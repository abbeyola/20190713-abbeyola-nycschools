//
//  SATView.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 16/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//

import UIKit

class SATView: UIView {

    let score: String
    let title: String
    private let titleLabel = UILabel()
    private let scoreCircleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor .constraint(equalToConstant: 64).isActive = true
        view.heightAnchor .constraint(equalToConstant: 64).isActive = true
        view.layer.cornerRadius = 32
        return view
    }()

    private let scoreLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: FontFamily.robotoBold, size: 20)
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()

    init(title: String, score: String) {
        self.title = title
        self.score = score
        super.init(frame: .zero)

        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setUp() {
        addSubview(titleLabel)
        addSubview(scoreCircleView)
        scoreCircleView.addSubview(scoreLabel)
        scoreCircleView.backgroundColor = getScoreBackgroundColor()

        titleLabel.text = title
        scoreLabel.text = score
        titleLabel.numberOfLines = 0
        titleLabel.textColor = .white
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true

        scoreCircleView.translatesAutoresizingMaskIntoConstraints = false
        scoreCircleView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        scoreCircleView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true

        scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        scoreLabel.centerYAnchor.constraint(equalTo: scoreCircleView.centerYAnchor).isActive = true
        scoreLabel.centerXAnchor.constraint(equalTo: scoreCircleView.centerXAnchor).isActive = true
    }

    func getScoreBackgroundColor() -> UIColor {
        guard let numericScore = Int(score) else { return .red }
        if numericScore < 100 {
            return .red
        } else if numericScore < 300 {
            return .blue
        } else if numericScore < 400 {
            return .darkGray
        } else {
            return .black
        }
    }
}

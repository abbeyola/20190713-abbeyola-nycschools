//
//  schoolCell.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 15/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//

import UIKit

class SchoolCell: UITableViewCell {

    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: FontFamily.robotoBold, size: 20)
        label.textAlignment = .left
        return label
    }()

    let cityLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: FontFamily.robotoLight, size: 14)
        label.textAlignment = .left
        label.textColor = .gray
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.addSubview(nameLabel)
        contentView.addSubview(cityLabel)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setUp() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.topAnchor.constraint(equalToSystemSpacingBelow: topAnchor, multiplier: 1).isActive = true
        nameLabel.leadingAnchor.constraint(equalToSystemSpacingAfter: leadingAnchor, multiplier: 1).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true

        cityLabel.translatesAutoresizingMaskIntoConstraints = false
        cityLabel.topAnchor.constraint(equalToSystemSpacingBelow: nameLabel.bottomAnchor, multiplier: 1).isActive = true
        cityLabel.leadingAnchor.constraint(equalToSystemSpacingAfter: nameLabel.leadingAnchor, multiplier: 0).isActive = true
    }
}

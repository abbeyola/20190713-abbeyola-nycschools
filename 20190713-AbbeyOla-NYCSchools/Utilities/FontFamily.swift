//
//  FontFamily.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 15/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//

struct FontFamily {
    static let robotoBold = "Roboto-Bold"
    static let robotoLight = "Roboto-Light"
}

//
//  Constant.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 17/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//

struct Constant {
    static let overview = "Overview"
    static let satScore = "SAT Score"
    static let numberOfTestTakers = "Number of test takers"
    static let averageMathScore = "Average maths score"
    static let averageCritcalScore = "Average Critical reading score"
    static let averageWritingScore = "Average Writing test score"
    static let satDataUnavailable = "SAT data unavailable"
    static let eligibility = "Eligibility"
    static let website = "Website"
    static let schoolCell = "schoolCell"
    static let dataUnavailable = "Data is Unavailable"
}

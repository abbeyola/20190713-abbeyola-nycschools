//
//  AppService.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 15/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//

import UIKit
class AppService {

    private var apiService: ApiService?
    var networkTaskFinishWithError: (() -> ())?
    var updateLoadingState: (() -> ())?
    var didFinishNetworkTask: (() -> ())?

    init(apiService: ApiService) {
        self.apiService = apiService
    }

    var schools: [School]? {
        didSet {
            self.didFinishNetworkTask?()

            DispatchQueue.global(qos: .background).async { [weak self] in
                guard let self = self else { return }
                self.fetchSATResults()
            }
        }
    }

    var satResults: [SAT]?

    var error: Error? {
        didSet { self.networkTaskFinishWithError?() }
    }

    var isLoading: Bool = false {
        didSet { self.updateLoadingState?() }
    }

    func fetchSchools() {
        self.apiService?.requestSchoolsData(completion: { (schools, error) in
            self.isLoading = false
            if let error = error {
                self.error = error
                return
            }
            self.error = nil
            self.schools = schools
        })
    }
    func fetchSATResults() {
        self.apiService?.requestSATData(completion: { (satResults, error) in
            self.isLoading = false
            if let error = error {
                self.error = error
                return
            }
            self.error = nil
            self.satResults = satResults
        })
    }
}


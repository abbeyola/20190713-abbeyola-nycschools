//
//  ApiService.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 15/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//
import UIKit

struct ApiService {

    static let shared = ApiService()
    private let schoolEndPoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    private let satEndpoint  = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

    func requestSchoolsData(completion: @escaping ([School]?, Error?) -> ()) {
        guard let url = URL(string: schoolEndPoint) else { return }
        let request = URLRequest(url:url)

        URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard let data = data else { return }

            do {
                let schools = try JSONDecoder().decode([School].self, from: data)
                DispatchQueue.main.async {
                    completion(schools, nil)
                }
            } catch let jsonErr {
                completion(nil, jsonErr)
            }
        }.resume()
    }

    func requestSATData(completion: @escaping ([SAT]?, Error?) -> ()) {
        guard let url = URL(string: satEndpoint) else { return }
        let request = URLRequest(url:url)

        URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard let data = data else { return }

            do {
                let satResults = try JSONDecoder().decode([SAT].self, from: data)
                DispatchQueue.main.async {
                    completion(satResults, nil)
                }
            } catch let jsonErr {
                completion(nil, jsonErr)
            }
        }.resume()
    }
}

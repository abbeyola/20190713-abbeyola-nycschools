//
//  AppDelegate.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 13/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}


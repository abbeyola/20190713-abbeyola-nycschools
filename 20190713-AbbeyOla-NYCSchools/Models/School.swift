//
//  School.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 14/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//

struct School {
    var school_name: String
    var dbn: String
    var boro: String
    var overview_paragraph: String
    var neighborhood: String
    var city: String
    var location: String
    var website: String
    var total_students: String
    var school_sports: String
    var latitude: String
    var longitude: String
    var eligibility1: String

    init(school_name: String, dbn: String, boro: String, overview_paragraph: String, neighborhood: String, city: String, location: String, website: String, total_students: String, school_sports: String, latitude: String, longitude: String, eligibility1: String) {
        self.school_name = school_name
        self.dbn = dbn
        self.boro = boro
        self.overview_paragraph = overview_paragraph
        self.neighborhood = neighborhood
        self.city = city
        self.location = location
        self.website = website
        self.total_students = total_students
        self.school_sports = school_sports
        self.latitude = latitude
        self.longitude = longitude
        self.eligibility1 = eligibility1
    }
}

extension School: Codable {
    enum SchoolCodingKeys: String, CodingKey {
        case school_name
        case dbn
        case boro
        case overview_paragraph
        case neighborhood
        case city
        case location
        case website
        case total_students
        case school_sports
        case latitude
        case longitude
        case eligibility1
    }

    init(from decoder: Decoder) throws {
        let noData = "Data is Unavailable"
        let container = try decoder.container(keyedBy: SchoolCodingKeys.self)
        let school_name = try container.decodeIfPresent(String.self, forKey: .school_name) ?? noData
        let dbn = try container.decodeIfPresent(String.self, forKey: .dbn) ?? noData
        let boro = try container.decodeIfPresent(String.self, forKey: .boro) ?? noData
        let overview_paragraph = try container.decodeIfPresent(String.self, forKey: .overview_paragraph) ?? noData
        let neighborhood = try container.decodeIfPresent(String.self, forKey: .neighborhood) ?? noData
        let city = try container.decodeIfPresent(String.self, forKey: .city) ?? noData
        let location = try container.decodeIfPresent(String.self, forKey: .location) ?? noData
        let website = try container.decodeIfPresent(String.self, forKey: .website) ?? noData
        let total_students = try container.decodeIfPresent(String.self, forKey: .total_students) ?? noData
        let school_sports = try container.decodeIfPresent(String.self, forKey: .school_sports) ?? noData
        let latitude = try container.decodeIfPresent(String.self, forKey: .latitude) ?? noData
        let longitude = try container.decodeIfPresent(String.self, forKey: .longitude) ?? noData
        let eligibility1 =   try container.decodeIfPresent(String.self, forKey: .eligibility1) ?? noData

        self.init(school_name: school_name, dbn: dbn, boro: boro, overview_paragraph: overview_paragraph, neighborhood: neighborhood, city: city, location: location, website: website, total_students: total_students, school_sports: school_sports, latitude: latitude, longitude: longitude, eligibility1: eligibility1)
    }
}

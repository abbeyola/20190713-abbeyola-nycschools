//
//  SAT.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 15/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//
struct SAT {
    var dbn: String
    var school_name: String
    var num_of_sat_test_takers: String
    var sat_critical_reading_avg_score: String
    var sat_math_avg_score: String
    var sat_writing_avg_score: String

    init(dbn: String, school_name: String, num_of_sat_test_takers: String, sat_critical_reading_avg_score: String, sat_math_avg_score: String, sat_writing_avg_score: String) {
        self.dbn = dbn
        self.school_name = school_name
        self.num_of_sat_test_takers = num_of_sat_test_takers
        self.sat_critical_reading_avg_score = sat_critical_reading_avg_score
        self.sat_writing_avg_score = sat_writing_avg_score
        self.sat_math_avg_score = sat_math_avg_score
    }
}

extension SAT: Codable {
    enum SATCodingKeys: String, CodingKey {
        case school_name
        case dbn
        case num_of_sat_test_takers
        case sat_critical_reading_avg_score
        case sat_math_avg_score
        case sat_writing_avg_score
    }

    init(from decoder: Decoder) throws {
        let noData = Constant.dataUnavailable
        let container = try decoder.container(keyedBy: SATCodingKeys.self)
        let dbn = try container.decodeIfPresent(String.self, forKey: .dbn) ?? noData
        let school_name = try container.decodeIfPresent(String.self, forKey: .school_name) ?? noData
        let num_of_sat_test_takers = try container.decodeIfPresent(String.self, forKey: .num_of_sat_test_takers) ?? noData
        let sat_critical_reading_avg_score = try container.decodeIfPresent(String.self, forKey: .sat_critical_reading_avg_score) ?? noData
        let sat_math_avg_score = try container.decodeIfPresent(String.self, forKey: .sat_math_avg_score) ?? noData
        let sat_writing_avg_score = try container.decodeIfPresent(String.self, forKey: .sat_writing_avg_score) ?? noData

        self.init(dbn: dbn, school_name: school_name, num_of_sat_test_takers: num_of_sat_test_takers, sat_critical_reading_avg_score: sat_critical_reading_avg_score, sat_math_avg_score: sat_math_avg_score, sat_writing_avg_score: sat_writing_avg_score)
    }
}


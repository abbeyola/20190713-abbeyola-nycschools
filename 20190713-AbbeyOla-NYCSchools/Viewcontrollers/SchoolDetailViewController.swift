//
//  SchoolDetailViewController.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 16/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController, UIScrollViewDelegate {
    lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "school"))
        imageView.clipsToBounds = true

        return imageView
    }()

    var imageHeightConstraint: NSLayoutConstraint!

    lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .brown
        view.contentInsetAdjustmentBehavior = .never
        view.contentInset = UIEdgeInsets(top: 200, left: 0, bottom: 0, right: 0)

        return view
    }()

    let schoolNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: FontFamily.robotoBold, size: 20)
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()

    lazy var writing = UIView()
    lazy var noSATDataLabel = UILabel()
    var websiteLabel = UILabel()
    var satResult: SAT?
    var school: School!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = 100 - (scrollView.contentOffset.y + 100)
        let height = max(y, 100)
        self.imageHeightConstraint.constant = height
    }

    func setupSubviews() {
        scrollView.delegate = self
        view.addSubview(self.scrollView)
        scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1)

        view.addSubview(self.imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        imageHeightConstraint = self.imageView.heightAnchor.constraint(equalToConstant: 100)
        imageHeightConstraint.isActive = true

        scrollView.contentOffset = CGPoint(x: 0, y: -400)
        scrollView.addSubview(schoolNameLabel)
        schoolNameLabel.text = school.school_name
        schoolNameLabel.translatesAutoresizingMaskIntoConstraints = false
        schoolNameLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
        schoolNameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        schoolNameLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 8).isActive = true

        let cityLabel = UILabel()
        cityLabel.text = school.city
        cityLabel.font = UIFont(name: FontFamily.robotoLight, size: 16)
        cityLabel.textAlignment = .center
        cityLabel.textColor = .white
        scrollView.addSubview(cityLabel)
        cityLabel.translatesAutoresizingMaskIntoConstraints = false
        cityLabel.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor, constant: 8).isActive = true
        cityLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        cityLabel.topAnchor.constraint(equalTo: self.schoolNameLabel.bottomAnchor, constant: 4).isActive = true

        let overviewTitleLabel = titleLabel(title: Constant.overview)
        scrollView.addSubview(overviewTitleLabel)
        overviewTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        overviewTitleLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
        overviewTitleLabel.topAnchor.constraint(equalTo: cityLabel.bottomAnchor, constant: 16).isActive = true

        let overviewLabel = contentLabel(title: school.overview_paragraph)
        scrollView.addSubview(overviewLabel)
        overviewLabel.translatesAutoresizingMaskIntoConstraints = false
        overviewLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
        overviewLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        overviewLabel.topAnchor.constraint(equalTo: overviewTitleLabel.bottomAnchor, constant: 8).isActive = true


        let satTitleLabel = titleLabel(title: Constant.satScore)
        scrollView.addSubview(satTitleLabel)
        satTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        satTitleLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
        satTitleLabel.topAnchor.constraint(equalTo: overviewLabel.bottomAnchor, constant: 16).isActive = true

        if let satResult = satResult {
            let testTaker = SATView(title: Constant.numberOfTestTakers, score: satResult.num_of_sat_test_takers)
            scrollView.addSubview(testTaker)
            testTaker.translatesAutoresizingMaskIntoConstraints = false
            testTaker.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1)
            testTaker.heightAnchor.constraint(equalToConstant: 100).isActive = true
            testTaker.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
            testTaker.topAnchor.constraint(equalTo: satTitleLabel.bottomAnchor, constant: 8).isActive = true
            testTaker.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true

            let mathScore = SATView(title: Constant.averageMathScore, score: satResult.sat_math_avg_score)
            scrollView.addSubview(mathScore)
            mathScore.translatesAutoresizingMaskIntoConstraints = false
            mathScore.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1)
            mathScore.heightAnchor.constraint(equalToConstant: 100).isActive = true
            mathScore.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
            mathScore.topAnchor.constraint(equalTo: testTaker.bottomAnchor, constant: 0).isActive = true
            mathScore.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true

            let criticalReading = SATView(title: Constant.averageCritcalScore, score: satResult.sat_critical_reading_avg_score)
            scrollView.addSubview(criticalReading)
            criticalReading.translatesAutoresizingMaskIntoConstraints = false
            criticalReading.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1)
            criticalReading.heightAnchor.constraint(equalToConstant: 100).isActive = true
            criticalReading.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
            criticalReading.topAnchor.constraint(equalTo: mathScore.bottomAnchor, constant: 0).isActive = true
            criticalReading.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true

            writing = SATView(title: Constant.averageWritingScore, score: satResult.sat_writing_avg_score)
            scrollView.addSubview(writing)
            writing.translatesAutoresizingMaskIntoConstraints = false
            writing.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1)
            writing.heightAnchor.constraint(equalToConstant: 100).isActive = true
            writing.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
            writing.topAnchor.constraint(equalTo: criticalReading.bottomAnchor, constant: 0).isActive = true
            writing.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        } else {
            noSATDataLabel = contentLabel(title: Constant.satDataUnavailable)
            scrollView.addSubview(noSATDataLabel)
            noSATDataLabel.translatesAutoresizingMaskIntoConstraints = false
            noSATDataLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
            noSATDataLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
            noSATDataLabel.topAnchor.constraint(equalTo: satTitleLabel.bottomAnchor, constant: 8).isActive = true
        }

        let eligibilityTitleLabel = titleLabel(title: Constant.eligibility)
        scrollView.addSubview(eligibilityTitleLabel)
        eligibilityTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        eligibilityTitleLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
        eligibilityTitleLabel.topAnchor.constraint(equalTo: satResult == nil ? noSATDataLabel.bottomAnchor : writing.bottomAnchor, constant: 8).isActive = true

        let eligibilityLabel = contentLabel(title: school.eligibility1)
        scrollView.addSubview(eligibilityLabel)
        eligibilityLabel.translatesAutoresizingMaskIntoConstraints = false
        eligibilityLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
        eligibilityLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        eligibilityLabel.topAnchor.constraint(equalTo: eligibilityTitleLabel.bottomAnchor, constant: 8).isActive = true


        let websiteTitleLabel = titleLabel(title: Constant.website)
        scrollView.addSubview(websiteTitleLabel)
        websiteTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        websiteTitleLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
        websiteTitleLabel.topAnchor.constraint(equalTo: eligibilityLabel.bottomAnchor, constant: 16).isActive = true

        websiteLabel = contentLabel(title: school.website)
        websiteLabel.textAlignment = .center
        scrollView.addSubview(websiteLabel)
        websiteLabel.translatesAutoresizingMaskIntoConstraints = false
        websiteLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8).isActive = true
        websiteLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        websiteLabel.topAnchor.constraint(equalTo: websiteTitleLabel.bottomAnchor, constant: 8).isActive = true

        let urlTapGesture = UITapGestureRecognizer(target: self, action: #selector(openUrl))
        websiteLabel.addGestureRecognizer(urlTapGesture)
        websiteLabel.isUserInteractionEnabled = true
    }

    @objc func openUrl() {
        guard let url = URL(string: "http://" + school.website) else { return }
        UIApplication.shared.open(url)
    }

    func titleLabel(title: String) -> UILabel {
        let label = UILabel()
        label.text = title
        label.font = UIFont(name: FontFamily.robotoBold, size: 18)
        label.textAlignment = .left
        label.textColor = .white

        return label
    }

    func contentLabel(title: String) -> UILabel {
        let label = UILabel()
        label.text = title
        label.textAlignment = .left
        label.textColor = .white
        label.numberOfLines = 0

        return label
    }

    override func viewDidAppear(_ animated: Bool) {
        scrollView.contentSize = CGSize(width: 0, height: websiteLabel.frame.origin.y + 80)
    }
}

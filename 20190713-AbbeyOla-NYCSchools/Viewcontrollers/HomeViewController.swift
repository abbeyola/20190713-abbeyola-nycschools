//
//  ViewController.swift
//  20190713-AbbeyOla-NYCSchools
//
//  Created by Abbey Ola on 13/07/2019.
//  Copyright © 2019 Abbey. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let schoolTableView = UITableView()

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appService.schools?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.schoolCell, for: indexPath) as? SchoolCell, let schools = self.appService.schools else { return UITableViewCell() }

        cell.nameLabel.text = schools[indexPath.row].school_name
        cell.cityLabel.text = schools[indexPath.row].city

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let school = appService.schools?[indexPath.row], let satResults = appService.satResults else { return }
        let schoolDetailVC = SchoolDetailViewController()
        for satResult in satResults where satResult.dbn == school.dbn {
            schoolDetailVC.satResult = satResult
        }
        schoolDetailVC.school = school
        self.navigationController?.pushViewController(schoolDetailVC, animated: true)
    }


    let appService = AppService(apiService: ApiService())

    override func viewDidLoad() {
        super.viewDidLoad()
        addSpinner()
        loadSchools()

        schoolTableView.register(SchoolCell.self, forCellReuseIdentifier: Constant.schoolCell)
        schoolTableView.delegate = self
        schoolTableView.dataSource = self
        schoolTableView.separatorStyle = .none

        setUpSubviews()
    }

    func setUpSubviews() {
        view.addSubview(schoolTableView)

        schoolTableView.translatesAutoresizingMaskIntoConstraints = false
        schoolTableView.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 1).isActive = true
        schoolTableView.trailingAnchor.constraint(equalToSystemSpacingAfter: view.trailingAnchor, multiplier: 1).isActive = true
        schoolTableView.topAnchor.constraint(equalToSystemSpacingBelow: view.topAnchor, multiplier: 2).isActive = true
        schoolTableView.bottomAnchor.constraint(equalToSystemSpacingBelow: view.bottomAnchor, multiplier: 1).isActive = true
    }

    private func loadSchools() {
        appService.fetchSchools()

        appService.updateLoadingState = {
            let _ = self.appService.isLoading ? self.activityIndicatorStart() : self.activityIndicatorStop()
        }

        appService.networkTaskFinishWithError = {
            if let error = self.appService.error {
                print(error.localizedDescription)
            }
        }

        appService.didFinishNetworkTask = {
            self.schoolTableView.reloadData()
            self.schoolTableView.separatorStyle = .singleLine
        }
    }

    let spinner = UIActivityIndicatorView(style: .whiteLarge)

    private func addSpinner() {
        spinner.color = .black
        spinner.center = view.center
        schoolTableView.addSubview(spinner)
        activityIndicatorStart()
    }

    private func activityIndicatorStart() {
        spinner.startAnimating()
    }

    private func activityIndicatorStop() {
        spinner.stopAnimating()
    }
}

